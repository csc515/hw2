package edu.missouristate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EchoController {

	@ResponseBody
	@RequestMapping(value = "/echo", 
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, 
			method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, 
					RequestMethod.DELETE, RequestMethod.OPTIONS})
	public Object echoBack(@RequestBody(required = false) Map<String, Object> payload, HttpServletRequest request)
			throws IOException {
		
		if ("POST".equalsIgnoreCase(request.getMethod().toString())) {
			return payload;
		} else {
			Map<String, String> correctedMap = getCorrectedParameterMap(request.getParameterMap()); 
			return correctedMap;
		}
	}

	private Map<String, String> getCorrectedParameterMap(Map<String, String[]> parameterMap) {		
		Map<String, String> params = new HashMap<String, String>();
		
		parameterMap.forEach((key,value) -> { 
			params.put(key, ((value[0]== null) ? "" : value[0])); 
		});
		
		return params;
	}
}
